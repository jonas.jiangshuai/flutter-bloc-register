import 'dart:async';

import 'bloc_provider.dart';


enum Event { authenticated, unauthorized }

class AuthenticationBloc extends BlocBase {

  final stateController = StreamController<Event>.broadcast();
  Stream<Event> get state => stateController.stream;

  void toRegister() {
    stateController.sink.add(Event.unauthorized);
  }
  void toHome() {
    stateController.sink.add(Event.authenticated);
  }
  @override
  void dispose() {
    stateController.close();
  }
}