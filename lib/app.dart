import 'package:flutter/material.dart';
import 'package:flutter_bloc_register/home/home_page.dart';
import 'package:flutter_bloc_register/register/register_page.dart';
import 'authentication_bloc.dart';
import 'bloc_provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
          child: const MyHomePage(),
          blocBuilder: () {
            return AuthenticationBloc();
          }),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    AuthenticationBloc bloc = BlocProvider.of<AuthenticationBloc>(context);
    return StreamBuilder(
        stream: bloc.state,
        initialData: Event.unauthorized,
        builder: (BuildContext context, AsyncSnapshot<Event> snapshot) {
          switch (snapshot.data) {
            case Event.authenticated:
              return const HomePage();
            default:
              return const RegisterPage();
          }
        });
  }
}
