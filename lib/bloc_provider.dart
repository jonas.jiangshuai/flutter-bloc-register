import 'package:flutter/cupertino.dart';

typedef BlocBuilder<T> = T Function();
typedef BlocDisposer<T> = Function(T);

abstract class BlocBase {
  void dispose();
}

class BlocProvider<T extends BlocBase> extends StatefulWidget {
  const BlocProvider({
    Key? key,
    required this.child,
    required this.blocBuilder,
    this.blocDispose,
  }) : super(key: key);

  final Widget child;
  final BlocBuilder<T> blocBuilder;
  final BlocDisposer<T>? blocDispose;

  @override
  State<BlocProvider<T>> createState() => _BlocProviderState<T>();

  static T of<T extends BlocBase>(BuildContext context) {
    // 问题2：在常见的基于InheritedWidget的widget, 比如 Theme.of
    // 都是用 dependOnInheritedWidgetOfExactType
    // 而在此处用的getElementForInheritedWidgetOfExactType，它们的区别是什么？
    _BlocProviderInherited<T>? provider = context
        .getElementForInheritedWidgetOfExactType<_BlocProviderInherited<T>>()
        ?.widget as _BlocProviderInherited<T>?;

    assert(provider != null);
    if (provider == null) {
      throw Exception("can not found _BlocProviderInherited");
    }

    return provider.bloc;
  }
}

class _BlocProviderState<T extends BlocBase> extends State<BlocProvider<T>> {
  late T bloc;

  @override
  void initState() {
    super.initState();
    bloc = widget.blocBuilder();
  }

  @override
  void dispose() {
    if (widget.blocDispose != null) {
      widget.blocDispose!(bloc);
    } else {
      bloc.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _BlocProviderInherited<T>(
      bloc: bloc,
      child: widget.child,
    );
  }
}

class _BlocProviderInherited<T> extends InheritedWidget {
  const _BlocProviderInherited({
    Key? key,
    required Widget child,
    required this.bloc,
  }) : super(key: key, child: child);

  final T bloc;

  // 问题1：updateShouldNotify 为什么永远返回 false?
  @override
  bool updateShouldNotify(_BlocProviderInherited oldWidget) => false;
}