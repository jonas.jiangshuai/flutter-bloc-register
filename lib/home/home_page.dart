import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../authentication_bloc.dart';
import '../bloc_provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthenticationBloc bloc = BlocProvider.of<AuthenticationBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Page"),
      ),
      body: Column(children: <Widget>[
        const Text("This is home page"),
        ElevatedButton(
          onPressed: () {
            bloc.toRegister();
          },
          child: const Text("back to register page"),
        )
      ]),
    );
  }
}
