import 'package:flutter/material.dart';
import '../authentication_bloc.dart';
import 'register_bloc.dart';
import '../bloc_provider.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RegisterBloc registerBloc = RegisterBloc();
    AuthenticationBloc authBloc = BlocProvider.of<AuthenticationBloc>(context);

    return BlocProvider(
        blocBuilder: () {
          return registerBloc;
        },
        child: Scaffold(
          appBar: AppBar(title: const Text("Register page")),
          body: Center(
            child: Column(
              children: <Widget>[
                TextField(
                  onChanged: (account) => registerBloc.onAccountChange(account),
                  decoration: const InputDecoration(
                    labelText: 'email',
                  ),
                ),
                TextField(
                  onChanged: (password) =>
                      registerBloc.onPasswordChange(password),
                  obscureText: true,
                  decoration: const InputDecoration(
                    labelText: 'password',
                  ),
                ),
                TextField(
                  onChanged: (password) =>
                      registerBloc.onPasswordConfirmChange(password),
                  obscureText: true,
                  decoration: const InputDecoration(
                    labelText: 'confirm password',
                  ),
                ),
                StreamBuilder(
                    stream: registerBloc.inputStream,
                    initialData: "",
                    builder: (BuildContext context, AsyncSnapshot<String> event) {
                      return Text(event.data??"", style: const TextStyle(color: Colors.red),
                      );
                    }),
                StreamBuilder(
                    stream: registerBloc.buttonStateStream,
                    initialData: registerBloc.defaultRegisterStateEvent,
                    builder:
                        (BuildContext context, AsyncSnapshot<RegisterStateEvent> event) {
                      Color bgColor = Colors.grey;
                      switch (event.data?.state) {
                        case RegisterState.registrable:
                          bgColor = Colors.blue;
                          break;
                        case RegisterState.registering:
                          WidgetsBinding.instance.addPostFrameCallback((_){
                            showDialog(
                                context: context,
                                barrierDismissible:false,
                                builder: (BuildContext context) {
                                  return   WillPopScope(child: const Center(
                                    child: Text("Registering",style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
                                  )  , onWillPop: () async => false);
                                }
                            );
                          });
                          break;
                        case RegisterState.registerErr:
                          bgColor = Colors.blue;
                          WidgetsBinding.instance.addPostFrameCallback((_){
                            Navigator.pop(context);
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Center(
                                    child: Text("Register error , msg : ${event.data?.msg}",style: const TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
                                  );
                                }
                            );
                          });
                          break;
                        case RegisterState.registerSuccess:
                          Navigator.pop(context);
                          authBloc.toHome();
                          break;
                        default:
                          bgColor = Colors.grey;
                      }

                      return ElevatedButton(
                          onPressed: () => {
                            registerBloc.onClickRegister()
                          },
                          style: ElevatedButton.styleFrom(
                              primary: bgColor,
                              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                              textStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                          child:const Text("Register"));
                    }),

              ],
            ),
          ),
        ));
  }
}
