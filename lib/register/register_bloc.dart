import 'dart:async';
import 'package:flutter_bloc_register/register/register_model.dart';
import '../bloc_provider.dart';

enum InputState {
  def,
  emailFormatErr,
  passwordLengthErr,
  passwordConfirmErr,
}
enum RegisterState{
  registrable,
  notRegistrable,
  registering,
  registerErr,
  registerSuccess,
}


class InputEvent {
  InputState state;
  String msg;

  InputEvent(this.state, this.msg);
}
class RegisterStateEvent{
  RegisterState state;
  String msg;
  RegisterStateEvent(this.state,this.msg);
}

class RegisterBloc extends BlocBase {
  final RegisterStateEvent defaultRegisterStateEvent = RegisterStateEvent(RegisterState.notRegistrable, "");

  final inputStateController = StreamController<String>();
  final registerStateController = StreamController<RegisterStateEvent>();
  final accountController = StreamController<String>();
  final passwordController = StreamController<String>();
  final passwordConfirmController = StreamController<String>();
  final RegExp _regexEmail = RegExp("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}\$");
  InputState currentInputState = InputState.def;
  RegisterState currentRegisterState = RegisterState.notRegistrable;
  String account = "";
  String password = "";
  String passwordConfirm = "";
  bool accountValidated = false;
  bool passwordValidated = false;
  bool passwordConfirmValidated = false;

  Stream<String> get inputStream => inputStateController.stream;
  Stream<RegisterStateEvent> get buttonStateStream => registerStateController.stream;

  RegisterBloc() {
    accountController.stream.listen((account) {
      this.account = account;
      accountValidated = _verifyAccount();
      _checkState();
    });
    passwordController.stream.listen((password) {
      this.password = password;
      passwordValidated = _verifyPassword();
      _checkState();
    });
    passwordConfirmController.stream.listen((passwordConfirm) {
      this.passwordConfirm = passwordConfirm;
      passwordConfirmValidated = _verifyPasswordConfirm();
      _checkState();
    });
  }

  _checkState() {
    RegisterState registerState = RegisterState.notRegistrable;
    if (!accountValidated) {
      _addInputEvent(InputState.emailFormatErr, "Email format error.");
    } else if (!passwordValidated) {
      _addInputEvent(InputState.passwordLengthErr, "Password too short.");
    } else if (!passwordConfirmValidated) {
      _addInputEvent(InputState.passwordConfirmErr, "Confirm password error.");
    } else {
      _addInputEvent(InputState.def,"");
      registerState =  RegisterState.registrable;
    }
    if(currentRegisterState != registerState){
      currentRegisterState = registerState;
      registerStateController.sink.add(RegisterStateEvent(currentRegisterState, ""));
    }
  }
  void _addInputEvent(InputState state,String msg){
    //状态发生变化才发送事件，避免ui频繁刷新
    if (currentInputState != state) {
      currentInputState = state;
      inputStateController.sink.add(msg);
    }
  }

  bool _verifyAccount() {
    return account.isNotEmpty && _regexEmail.hasMatch(account);
  }

  bool _verifyPassword() {
    return password.length >= 6;
  }

  bool _verifyPasswordConfirm() {
    return passwordValidated && password == passwordConfirm;
  }

  void _register(Function success, Function error) {
    if (currentInputState == InputState.def) {
      registerStateController.add(RegisterStateEvent(RegisterState.registering, ""));
      register(success, error);
    }
  }

  void onAccountChange(String account) {
    accountController.sink.add(account);
  }

  void onPasswordChange(String password) {
    passwordController.sink.add(password);
  }

  void onPasswordConfirmChange(String passwordConfirm) {
    passwordConfirmController.sink.add(passwordConfirm);
  }

  void onClickRegister() {
    registerStateController.sink.add(RegisterStateEvent(RegisterState.registering, ""));
    _register(() {
      registerStateController.sink.add(RegisterStateEvent(RegisterState.registerSuccess, ""));
    }, (String errMsg) {
      inputStateController.sink.add("");
      registerStateController.sink.add(RegisterStateEvent(RegisterState.registerErr, errMsg));
    });
  }

  @override
  void dispose() {
    inputStateController.close();
    registerStateController.close();
    accountController.close();
    passwordController.close();
    passwordConfirmController.close();
  }
}
