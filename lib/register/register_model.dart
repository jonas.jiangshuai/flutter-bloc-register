import 'dart:math';

void register(Function success, Function error) {
  Future.delayed(const Duration(seconds: 2), () {
    int nextInt = Random().nextInt(100);
    if (nextInt > 50) {
      success();
    } else {
      error("Register timeout");
    }
  });
}